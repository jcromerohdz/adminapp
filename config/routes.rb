Rails.application.routes.draw do
  devise_for :users, controllers: { confirmations: 'confirmations' }
  root 'welcome#index'

  resources :users, only: [:index, :show]

  get 'static_page/landing_page'
  #get 'static_page/privacy_policy'
  get "privacy_polecy", to: "static_page#privacy_policy"
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
